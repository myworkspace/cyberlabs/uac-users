<?php

return [

    'login' => [
        'title' => 'Login',
        'label' => 'Masukkan email dan password Anda',
        'fields' => [
            'email' => 'Email',
            'password' => 'Password',
            'remember_me' => 'Ingat saya',
        ],
    ],

    'user' => [
        'fields' => [
            'name' => 'Name',
            'email' => 'Email',
            'action' => 'Aksi',
            'level' => 'Level',
            'password' => 'Password',
            'password_change' => 'Ubah Password',
            'password_change_nb' => 'Kosongkan jika tidak ingin diubah',
        ],
        'message' => [
            'store' => 'Data berhasil ditambahkan.',
            'update' => 'Data berhasil diperbarui.',
            'delete' => 'Data berhasil dihapus.'
        ]
    ],

];