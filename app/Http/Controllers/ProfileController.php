<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Hash;
use Auth;

class ProfileController extends Controller
{
    
    public function edit(){
        
        $user = User::find(Auth::user()->id);

        return view('user.form', [
            'title' => __('menus.profile'),
            'user' => $user,
            'edit' => true,
            'profile' => true,
        ]);

    }

    public function update(Request $request){

        $id = Auth::user()->id;

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id,
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('profile')
                    ->withErrors($validator)
                    ->withInput();
        }

        $update = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ];

        if($request->input('password'))
            $update['password'] = Hash::make($request->input('password'));

        User::where('id', $id)->update($update);

        return redirect()
                ->route('profile')
                ->withSuccess(__('pages.user.message.update'));
    }

}
