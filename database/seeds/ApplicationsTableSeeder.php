<?php

use Illuminate\Database\Seeder;
use App\Models\Application;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'BKM Express',
                'code' => 'bkm3xpr3s5',
                'secret_key' => '352b2010402bc858a08d0524d9ce33d5',
                'redirect' => 'http://bkmexpress.uac:8001/login',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Messaging',
                'code' => 'm3ss4g1n6',
                'secret_key' => '6dd5452492381aea23c90f8bd374fc49',
                'redirect' => 'http://messaging.uac:8002/login',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

        Application::insert($data);
    }
}
