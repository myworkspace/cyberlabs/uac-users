<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Application;
use App\Models\Level;
use App\Models\Token;
use Validator;
use Hash;
use Auth;

class UserController extends Controller
{

    public function gate(Request $request){
        $code = $request->input('code');
        $key = $request->input('key');

        $app = Application::where('code', $code)->first();
        if($app){
            $raw = $app->code . $app->secret_key . 'request' . date("Ymd");
            $app_key = hash("sha1", $raw);

            if($app_key == $key){

                if(!session('token')){

                    $raw = $app->code . $app->secret_key . 'token' . date("YmdHis");
                    $token = hash("sha1", $raw);
                    session(['token' => $token]);
                    
                    $create = [
                        'token' => $token,
                        'user_id' => Auth::user()->id,
                        'expired_at' => date("Y-m-d H:i:s", strtotime('+6 hours')),
                    ];

                    Token::create($create);

                } else {

                    $token = session('token');

                }

                $raw = $app->code . $app->secret_key . 'accepted' . date("Ymd");
                $new_key = hash("sha1", $raw);
                
                return redirect()->to($app->redirect . '?key=' . $new_key . '&token=' . $token);
            }
        }

        if(Auth::user()->level_id == 1){
            return redirect()->route('user', ['level' => 2]);
        } else {
            return redirect()->route('logout');
        }
    }

    public function data(Request $request){

        $key = $request->input('token');
        $token = Token::with(['user'])->where('token', $key)->orderBy('id', 'desc')->first();

        if($token){

            if($token->expired_at >= date("Y-m-d H:i:s"))
                return response()->json($token->user, 200);

            Token::where('id', $token->id)->delete();
            session()->forget('token');
            session()->flush();

        }

        return response()->json('Unauthenticated.', 401);
        
    }

    public function index(Request $request, $level) {

        $page = $request->input('page');
        $limit = 15;
        $num = 0;
        if($page > 0) $num = ($page - 1) * $limit;

        $title = Level::find($level)->name;

        $users = User::where('level_id', $level)->paginate($limit);
        return view('user.index', [
            'users' => $users,
            'num' => $num,
            'title' => $title,
            'level' => $level,
        ]);
    }

    public function add($level){
        
        $form = $this->initForm($level, 'add');
        return view('user.form', $form);

    }

    public function store(Request $request, $level){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'level_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('user.add', ['level' => $level])
                    ->withErrors($validator)
                    ->withInput();
        }

        $create = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'level_id' => $request->input('level_id'),
            'password' => Hash::make($request->input('password')),
        ];

        User::create($create);

        return redirect()
                ->route('user', ['level' => $level])
                ->withSuccess(__('pages.user.message.store'));

    }

    public function edit($level, $id){

        $form = $this->initForm($level, 'edit');
        $user = User::find($id);

        return view('user.form', array_merge($form, [
            'user' => $user,
            'edit' => true,
        ]));

    }

    public function update(Request $request, $level = null, $id = null){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id,
            'level_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('user.edit', ['level' => $level, 'id' => $id])
                    ->withErrors($validator)
                    ->withInput();
        }

        $update = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'level_id' => $request->input('level_id'),
        ];

        if($request->input('password'))
            $update['password'] = Hash::make($request->input('password'));

        User::where('id', $id)->update($update);

        return redirect()
                ->route('user', ['level' => $level])
                ->withSuccess(__('pages.user.message.update'));

    }

    public function delete(Request $request, $level, $id){
        User::where('id', $id)->delete();
        return back()
            ->withSuccess(__('pages.user.message.delete'));
    }

    private function initForm($level, $type){
        $title = Level::find($level)->name . ' <i class="material-icons">arrow_forward</i> ' . __('buttons.' . $type);
        $levels = Level::where('id', 2)->orWhere('id', 3)->get();

        return [
            'title' => $title,
            'levels' => $levels,
            'level_id' => $level,
        ];
    }

}
