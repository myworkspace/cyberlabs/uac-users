<?php

namespace App\Http\Middleware;

use Closure;

class AuthCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->user()->level_id != 1) {
            return redirect('logout');
        }

        return $next($request);
    }
}
