@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>{!! $title !!}</h2>
    </div>
    @if(!isset($profile))
    <div class="col-lg-2 text-lg-right mt-lg-0 mt-3">
        <a href="{{ url()->previous() }}" class="btn btn-outline-primary btn-icon btn-block">
            <i class="material-icons">arrow_back</i> @lang('buttons.back')
        </a>
    </div>
    @endif
</div>

@if(session('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
	{!! session('success') !!}
</div>
@endif

<form method="post" action="" id="form">

    @csrf

    @if(isset($edit))
        @method('PUT')
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="name">@lang('pages.user.fields.name')</label>
                <input name="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" value="{{ old('name') ? old('name') : (isset($user->name) ? $user->name : '') }}" required>
                @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email">@lang('pages.user.fields.email')</label>
                <input name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" value="{{ old('email') ? old('email') : (isset($user->email) ? $user->email : '') }}" required>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">{{ isset($edit) ? __('pages.user.fields.password_change') : __('pages.user.fields.password') }}</label>
                <input name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" {{ !isset($edit) ? 'required' : '' }}>
                @if(isset($edit))
                    <small id="emailHelp" class="form-text text-muted">@lang('pages.user.fields.password_change_nb')</small>
                @endif
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            @if(!isset($profile))
            <div class="form-group">
                <label for="level">@lang('pages.user.fields.level')</label>
                <select id="level" name="level_id" class="form-control">
                    @foreach($levels as $level)
                        <option {{ $level->id == $level_id ? 'selected' : '' }} value="{{ $level->id }}">{{ $level->name }}</option>
                    @endforeach
                </select>
            </div>
            @endif

            <button type="button" onclick="submitForm();" id="button" class="btn btn-primary mt-4">{{ isset($edit) ? __('buttons.update') : __('buttons.save') }}</button>
        </div>
        
    </div>

</form>

@endsection