@extends('layouts.app')

@section('body')

<div class="row justify-content-center">
    <div class="col-xl-5">
        <div class="card py-4 px-3">
            <div class="card-body">
                <form method="POST" action="">
                    @csrf
                    <img src="{{ asset('images/logo.png') }}" class="mb-4 w-100">

                    <h3>@lang('pages.login.title')</h3>
                    <p class="mb-2">@lang('pages.login.label')</p>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="email"><i class="material-icons">email</i></span>
                        </div>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="@lang('pages.login.fields.email')" id="email" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="password"><i class="material-icons">lock</i></span>
                        </div>
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="@lang('pages.login.fields.password')" id="password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="custom-control custom-checkbox mb-3">
                        <input type="checkbox" class="custom-control-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="remember">@lang('pages.login.fields.remember_me')</label>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block">@lang('buttons.login')</button>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection