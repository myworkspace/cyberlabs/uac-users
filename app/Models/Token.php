<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{

    protected $fillable = [
        'token',
        'user_id',
        'expired_at',
    ];
    
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
