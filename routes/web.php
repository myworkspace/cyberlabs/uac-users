<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use App\Models\Token;
use App\Models\Application;

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/logout', function(Request $request){

    $app = null;
    if($request->input('code')){
        $app = Application::where('code', $request->input('code'))->first();
    }
    
    Token::where('token', session('token'))->delete();
    session()->forget('token');
    session()->flush();
    Auth::logout();

    if($app){
        return redirect()->to($app->redirect);
    } else {
        return redirect()->route('login');
    }

})->name('logout');

Route::get('/user', function(){
    return redirect()->route('user', ['level' => 2]);
});

Route::post('/user', 'UserController@data')->name('user.data');

Route::group(['middleware' => 'auth'], function(){
    
    Route::get('/gate', 'UserController@gate')->name('user.gate');

    Route::group(['middleware' => 'auth.custom'], function(){
    
        Route::get('/profile', 'ProfileController@edit')->name('profile');
        Route::put('/profile', 'ProfileController@update')->name('profile.update');

        Route::group(['prefix' => 'user'], function(){
            
            $where = ['level' => '[2-3]', 'id' => '[0-9]+'];
            
            Route::get('/{level}', 'UserController@index')->name('user')->where($where);

            Route::get('/{level}/add', 'UserController@add')->name('user.add')->where($where);
            Route::post('/{level}/add', 'UserController@store')->name('user.store')->where($where);

            Route::get('/{level}/{id}', 'UserController@edit')->name('user.edit')->where($where);
            Route::put('/{level}/{id}', 'UserController@update')->name('user.update')->where($where);

            Route::delete('/{level}/{id}', 'UserController@delete')->name('user.delete')->where($where);

        });

    });

});