<?php

return [
    'users' => 'PENGGUNA',
    'sales_admin' => 'Sales Admin',
    'customer_service' => 'Customer Service',

    'account' => 'AKUN',
    'profile' => 'Profil',
    'logout' => 'Keluar',
];
