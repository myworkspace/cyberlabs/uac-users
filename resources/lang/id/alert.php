<?php

return [
    'confirmation' => [
        'delete' => 'Anda yakin menghapus :data ?',
        'logout' => 'Anda yakin keluar?',
    ],
];