<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{

    public function user(){
        return $this->hasMany('App\Models\User', 'level_id', 'id');
    }
    
}
