@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>{!! $title !!}</h2>
    </div>
    <div class="col-lg-2 text-lg-right mt-lg-0 mt-3">
        <a href="{{ route('user.add', ['level' => $level]) }}" class="btn btn-outline-primary btn-icon btn-block">
            <i class="material-icons">add</i> @lang('buttons.add')
        </a>
    </div>
</div>

@if(session('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
	{!! session('success') !!}
</div>
@endif

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">@lang('pages.user.fields.name')</th>
				<th scope="col">@lang('pages.user.fields.email')</th>
				<th scope="col" class="column-action-2 text-center">@lang('pages.user.fields.action')</th>
			</tr>
		</thead>
		<tbody>
		@foreach($users as $user)
			<tr>
				<th scope="row">{{ ++$num }}</th>
				<td>{{ $user->name }}</td>
				<td>{{ $user->email }}</td>
				<td class="text-right">
                    <a href="{{ route('user.edit', ['level' => $level, 'id' => $user->id]) }}" class="btn btn-sm btn-outline-success">@lang('buttons.edit')</a>

                    <form style="display: inline" id="form-{{ $num }}" action="{{ route('user.delete', ['level' => $level, 'id' => $user->id]) }}" method="post">
                        @method('DELETE')
                        @csrf
                        <button type="button" onclick="submitForm('#form-{{ $num }}', '#delete-{{ $num }}', '@lang('alert.confirmation.delete', ['data' => $user->name])');"  id="delete-{{ $num }}" class="btn btn-sm btn-outline-danger">@lang('buttons.delete')</button> 
                    </form>

				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

{{ $users->links() }}

@endsection
